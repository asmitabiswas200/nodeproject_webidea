const router = require( 'express' ).Router();
const userController = require( '../controllers/user.controller' );
const authenticate = require('../middleware/authenticate');

router.get('/:id', authenticate, userController.readById);
router.get('/', authenticate, userController.read);
router.post('/insert', authenticate, userController.insert);
router.post('/images', authenticate, userController.imageUpload);
router.put('/:id', authenticate, userController.update);
router.delete('/:id', authenticate, userController.remove);

module.exports = router;