const mongoose = require( 'mongoose' );

mongoose.connect('mongodb://localhost:27017/clientdb', {
    useNewUrlParser : true,
    useFindAndModify : true,
    useCreateIndex : true,
    useUnifiedTopology: true
});
const db = mongoose.connection;
db.on('error', (err)=> {
    console.log('error ocurred!');
});

db.once('open', ()=> {
    console.log('connection successful!')
});

//module.exports = connection;