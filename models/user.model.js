const mongoose = require( 'mongoose' );
const autoIncrement = require('mongoose-auto-increment');
// const db = require( '../db' );
// const connection = db.connection;
const connection = mongoose.createConnection("mongodb://localhost/clientdb",
{
    useNewUrlParser : true,
    useFindAndModify : true,
    useCreateIndex : true,
    useUnifiedTopology: true
});

autoIncrement.initialize(connection);

const Schema = mongoose.Schema;

const userSchema = new Schema({
    name : {
        type : String,
        required : true
    },
    email:{
        type : String,
        required : true
    },
    identityproof : {
        type : Number,
        required : true
    },
    phone: {
        type : String,
        required : true
    },
    address: {
        type : String,
        required : true
    },
    gender: {
        type : String,
        required : true
    },
    image : {
       type : String
    },
    designation : {
        type : String,
        required : true
    },
    joiningdate : {
        type : Date,
        required : true
    },
    employeeCode : {
        type : String,
        required : true,
        // get : (employeeCode) => {

        //     return `wis_${employeeCode}`
        // }
    }
}, {timestamps : true }); // toJSON : { getters : true }

userSchema.plugin(autoIncrement.plugin, {
    model: 'User',
    field: 'employeeCode',
    startAt: 001,
    incrementBy: 1
});

const User = connection.model('User', userSchema);
module.exports =  User ;