const express = require( 'express' );
const bodyParser = require( 'body-parser' );

const app = express();
const userRoutes = require( './routes/user.routes' );
const authRoutes = require( './routes/aut.route' );

require('./db');

app.use(bodyParser.urlencoded({extended : true }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
   res.json(`welcome to the application`);
});

//Imported Routes
app.use('/api/user',userRoutes);
app.use('/api/auth',authRoutes);

const port = process.env.port || 3000;

app.listen(port, () => {
    console.log(`app is running on ${port}`)
})