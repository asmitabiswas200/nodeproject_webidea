const jwt = require('jsonwebtoken');

const authenticate = async(req, res, next) => {
    try {
        const token = await req.headers.authorization.split(' ')[1];
        const decode = jwt.verify(token, 'secretValue');
        req.user = decode;
        next()
    } catch (error) {
        res.json({
            message : 'Authentication Failed!'
        })
    }
}
module.exports = authenticate;