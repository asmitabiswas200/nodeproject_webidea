const Auth = require( '../models/auth.model');
const bcrypt = require( 'bcryptjs' );
const jwt = require('jsonwebtoken');

const register = async(req, res, next) => {
    const hashedpass = await bcrypt.hash(req.body.password, 10);

    let user = new Auth({

        name : req.body.name,
        email : req.body.email,
        phone : req.body.phone,
        password : hashedpass
    });

    const result = await user.save();
    res.json({message : 'user registred successfully'});
};

const login = (req, res, next) => {
    var username = req.body.username
    var password = req.body.password

        Auth.findOne({$or : [{email : username}, {phone:username}]})
        .then(user => {
            if(user){
               bcrypt.compare(password, user.password, function(err, result){
                   if(err){
                       res.json({
                           error : err
                       })
                   }
                   if(result){
                       let token = jwt.sign({name : user.name}, 'secretValue',{expiresIn : '1h'})
                       res.json({
                           message : 'Login Successfully',
                           token
                       })
                  }else{
                      res.json({
                          message :  'Password does not matched'
                      })
                  }
               })
            }else{
                res.json({
                    message : 'No user found'
                })
            }
        })
}


module.exports = { register, login };