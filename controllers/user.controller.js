const User = require( '../models/user.model' );
const multer = require('multer');
const path = require('path');
// const upload = multer({dest : '/uploads/'});

const storage = multer.diskStorage({
   destination: (req,file,cb) => cb(null, 'uploads/'),
   filename: (req,file,cb) => {
       const uniqueName = `${Date.now()}-${Math.round(Math.random() * 1E9)}${path.extname(file.originalname)}`;
       cb(null, uniqueName);
   }
});

const handleMultipartdata = multer({storage, limits : { fileSize : 1000000 * 5 }}).single('image');


const imageUpload =  async(req, res, next) => {
        handleMultipartdata(req, res, (err) => {
            if(err) {
                res.status(401).json("error occured");
            };
            console.log(req.file);
            //const filePath = req.file.path;
            res.json({message : "Image uploaded successfully"});

        })
}


const insert =  async(req, res, next) => {

    let user = new User({
        name : req.body.name,
        email : req.body.email,
        identityproof : req.body.identityproof,
        phone : req.body.phone,
        address : req.body.address,
        gender : req.body.gender,
        designation : req.body.designation,
        joiningdate : req.body.joiningdate,
        employeeCode : req.body.employeeCode
    });
    console.log(user);
    try {
       const result = await user.save();
       res.status(200).json({
        message: 'Employee added successfully!'
    }) 
    } catch (error) {
        res.status(401).json({
            message: 'error occured!'
        })
    }
};

//Get all data
const read =  async(req, res, next) => {
    try {
        if ( req.query.search ) {
            const searchElem = req.query.search;
            const searchValue = { '$regex': searchElem, '$options': 'si' };
            req.query = { ...req.query, '$or': [ { 'email': searchValue }, { 'employeeCode': searchValue } ] };
        }
        delete req.query.search;

        const result = await User.find();
        res.status(201).json({result}); 
     } catch (error) {
         res.status(400).json({
             message: error.message
         })
     }
};

const readById = async(req, res, next) => {
    try {
        
    } catch (error) {
        
    }
}

const update = async(req, res, next)  => {
    const userId = req.params.userId;
    let user = {
        name : req.body.name,
        email : req.body.email,
        identityproof : req.body.identityproof,
        phone : req.body.phone,
        address : req.body.address,
        gender : req.body.gender,
        designation : req.body.designation,
        joiningdate : req.body.joiningdate,
        employeeCode : req.body.employeeCode
    };
    try {
       const result = await User.findOneAndUpdate(userId, {$set : user}, {new : true});
       res.status(200).json({result});

    } catch (error) {
        res.status(401).json({
            message: 'error occured!'
        })
    }
};

const remove = async(req, res, next) => {
    const userId = req.params.userId;
    try {
        const result = await User.findOneAndDelete(userId);
        res.status(200).json({result});
 
     } catch (error) {
         res.status(401).json({
             message: 'error occured!'
         })
     }
};

module.exports = { insert, read, readById, update, remove, imageUpload };